# Copyright © 2016-2017 Simon McVittie
# SPDX-License-Identifier: GPL-2.0+
# (see vectis/__init__.py)

import logging
import os
import subprocess
import typing

from debian.debian_support import (
    Version,
)

from vectis.error import ArgumentError
from vectis.worker import (
    VirtWorker,
)

if typing.TYPE_CHECKING:
    import vectis.config
    vectis.config       # appease pyflakes

logger = logging.getLogger(__name__)


def vmdebootstrap_argv(
        version,
        *,
        architecture,
        archive_access,
        components,
        debootstrap_version,
        include=(),
        kernel_package,
        merged_usr,
        qemu_image_size,
        suite,
        uri):
    default_name = 'autopkgtest.qcow2'
    argv = [
        'env',
        'AUTOPKGTEST_APT_PROXY={}'.format(
            archive_access.apt_proxy_for_suite(suite)),
        'MIRROR={}'.format(uri),
        'RELEASE={}'.format(suite),

        'vmdebootstrap',
        '--log=/dev/stderr',
        '--verbose',
        '--serial-console',
        '--distribution={}'.format(suite),
        '--user=user',
        '--hostname=host',
        '--sparse',
        '--size={}'.format(qemu_image_size),
        '--mirror={}'.format(uri),
        '--arch={}'.format(architecture),
        '--grub',
        '--no-extlinux',
    ]

    if kernel_package is not None:
        if version >= Version('1.4'):
            argv.append('--kernel-package={}'.format(kernel_package))
        else:
            argv.append('--no-kernel')
            argv.append('--package={}'.format(kernel_package))

    for package in include:
        argv.append('--package={}'.format(package))

    debootstrap_args = []

    debootstrap_args.append('components={}'.format(
        ','.join(components)))

    if debootstrap_version >= Version('1.0.86~'):
        if merged_usr:
            debootstrap_args.append('merged-usr')
            default_name = 'autopkgtest-merged-usr.qcow2'
        else:
            # piuparts really doesn't like merged /usr
            debootstrap_args.append('no-merged-usr')

    return argv, debootstrap_args, default_name


def new_vmdb2(
    *,
    architecture,               # type: str
    archive_access,             # type: vectis.config.ArchiveAccess
    default_dir,                # type: str
    out,                        # type: str
    storage,                    # type: str
    suite,                      # type: vectis.config.Suite
    uri,                        # type: str
    vendor,                     # type: vectis.config.Vendor
    vmdb2_worker,               # type: str
    vmdb2_worker_suite,         # type: vectis.config.Suite
):
    # type: (...) -> typing.Tuple[str, str]

    if out is None:
        out = os.path.join(default_dir, 'autopkgtest.qcow2')

    for suite in (vmdb2_worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    with VirtWorker(
        vmdb2_worker,
        archive_access=archive_access,
        storage=storage,
        suite=vmdb2_worker_suite,
    ) as worker:
        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get', '-y', 'upgrade',
        ])

        worker_packages = [
            'autopkgtest',
            'vmdb2',
        ]

        worker.check_call([
            'env',
            'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get',
            '-y',
            '-t', vmdb2_worker_suite.apt_suite,
            '--no-install-recommends',
            'install',
        ] + worker_packages)

        argv = [
            'autopkgtest-build-qemu',
            str(suite),
            '{}/output.qcow2'.format(worker.scratch),
            uri,
            architecture,
        ]

        # TODO: Run a second setup script? (Append it to argv)

        worker.check_call([
            'env',
            'DEBIAN_FRONTEND=noninteractive',
            'AUTOPKGTEST_APT_PROXY=DIRECT',
            worker.command_wrapper,
            '--',
        ] + argv)

        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/output.qcow2'.format(worker.scratch), out + '.new')

    return out + '.new', out


def new_ubuntu_cloud(
        *,
        architecture,
        default_dir,
        out,
        qemu_image_size,
        suite,
        uri,
        vendor):
    if out is None:
        out = os.path.join(default_dir, 'autopkgtest-cloud.qcow2')

    out_dir = os.path.dirname(out) or os.curdir
    argv = ['autopkgtest-buildvm-ubuntu-cloud']

    argv.append('--arch={}'.format(architecture))
    argv.append('--disk-size={}'.format(qemu_image_size))
    argv.append('--mirror={}'.format(uri))
    argv.append('--proxy=DIRECT')
    argv.append('--release={}'.format(suite))
    argv.append('--verbose')
    argv.append('--output-dir={}'.format(out_dir))

    image = '{}/autopkgtest-{}-{}.img'.format(out_dir, suite, architecture)

    try:
        subprocess.check_call(argv)
    except Exception:
        if os.path.exists(image):
            os.unlink(image)
        raise
    else:
        return image, out


def new(
        *,
        apt_key,
        apt_key_package,
        architecture,
        archive_access,
        components,
        default_dir,
        include=(),
        kernel_package,
        merged_usr,
        out,
        qemu_image_size,
        storage,
        suite,
        uri,
        vmdebootstrap_options,
        vmdebootstrap_worker,
        vmdebootstrap_worker_suite):

    for suite in (vmdebootstrap_worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    with VirtWorker(
        vmdebootstrap_worker,
        archive_access=archive_access,
        storage=storage,
        suite=vmdebootstrap_worker_suite,
    ) as worker:
        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get', '-y', 'upgrade',
        ])

        worker_packages = [
            'autopkgtest',
            'grub2-common',
            'python3',
            'qemu-utils',
            'vmdebootstrap',
        ]

        # Optional (x86 only, but necessary for wheezy)
        optional_worker_packages = [
            'extlinux',
            'mbr',
        ]

        keyring = apt_key_package

        if keyring is not None:
            optional_worker_packages.append(keyring)

        worker.check_call([
            'env',
            'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            'apt-get',
            '-y',
            '-t', vmdebootstrap_worker_suite.apt_suite,
            '--no-install-recommends',
            'install',
        ] + worker_packages)

        # Failure is ignored for these non-critical packages
        for p in optional_worker_packages:
            worker.call([
                'env',
                'DEBIAN_FRONTEND=noninteractive',
            ] + archive_access.get_proxy_env() + [
                'apt-get',
                '-y',
                '-t', vmdebootstrap_worker_suite.apt_suite,
                '--no-install-recommends',
                'install',
                p])

        version = worker.dpkg_version('vmdebootstrap')
        debootstrap_version = worker.dpkg_version('debootstrap')

        argv, debootstrap_args, default_name = vmdebootstrap_argv(
            version,
            architecture=architecture,
            archive_access=archive_access,
            components=components,
            debootstrap_version=debootstrap_version,
            include=include,
            kernel_package=kernel_package,
            qemu_image_size=qemu_image_size,
            suite=suite,
            uri=uri,
            merged_usr=merged_usr,
        )
        argv.extend(vmdebootstrap_options)

        if worker.call(['test', '-f', apt_key]) == 0:
            logger.info('Found apt key worker:{}'.format(apt_key))
            debootstrap_args.append('keyring={}'.format(apt_key))
        elif os.path.exists(apt_key):
            logger.info('Found apt key host:{}, copying to worker:{}'.format(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch)))
            worker.copy_to_guest(
                apt_key, '{}/apt-key.gpg'.format(worker.scratch))
            debootstrap_args.append('keyring={}/apt-key.gpg'.format(
                worker.scratch))
        else:
            logger.warning('Apt key host:{} not found; leaving it out and '
                           'hoping for the best'.format(apt_key))

        if debootstrap_args:
            argv.append('--debootstrapopts={}'.format(
                ' '.join(debootstrap_args)))

        worker.copy_to_guest(
            os.path.join(os.path.dirname(__file__), '..', 'setup-testbed'),
            '{}/setup-testbed'.format(worker.scratch))
        worker.check_call([
            'chmod', '0755', '{}/setup-testbed'.format(worker.scratch)])
        worker.check_call([
            'env', 'DEBIAN_FRONTEND=noninteractive',
        ] + archive_access.get_proxy_env() + [
            worker.command_wrapper,
            '--',
        ] + argv + [
            '--customize={}/setup-testbed'.format(worker.scratch),
            '--image={}/output.raw'.format(worker.scratch),
        ])

        worker.check_call([
            'qemu-img', 'convert', '-f', 'raw', '-O',
            'qcow2', '-c', '-p',
            '{}/output.raw'.format(worker.scratch),
            '{}/output.qcow2'.format(worker.scratch),
        ])

        if out is None:
            out = os.path.join(default_dir, default_name)

        os.makedirs(os.path.dirname(out) or os.curdir, exist_ok=True)
        worker.copy_to_host(
            '{}/output.qcow2'.format(worker.scratch), out + '.new')

    return out + '.new', out


def run(
    args,
    *,
    _cloud=False,
    _include=[],
    _keep=False,
    _merged_usr=False,
    _uri=None,
    _vmdb2=False,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    apt_key = args.apt_key
    apt_key_package = args.apt_key_package
    architecture = args.architecture
    archive_access = args.get_archive_access()
    cloud = _cloud
    components = args.components
    keep = _keep
    kernel_package = args.get_kernel_package(architecture)
    include = _include
    out = args.write_qemu_image
    qemu_image_size = args.qemu_image_size
    storage = args.storage
    uri = _uri
    vendor = args.vendor
    suite = args.suite
    vmdebootstrap_options = args.vmdebootstrap_options
    vmdebootstrap_worker = args.vmdebootstrap_worker
    vmdebootstrap_worker_suite = args.vmdebootstrap_worker_suite
    default_dir = os.path.join(
        storage, architecture, str(vendor), str(suite))

    for suite in (vmdebootstrap_worker_suite, suite):
        for ancestor in suite.hierarchy:
            archive_access.check_suite(ancestor)

    if uri is None:
        uri = archive_access.mirror_for_suite(suite)

    if _vmdb2:
        created, out = new_vmdb2(
            architecture=architecture,
            archive_access=archive_access,
            default_dir=default_dir,
            out=out,
            storage=storage,
            suite=suite,
            uri=uri,
            vendor=vendor,
            vmdb2_worker=args.vmdb2_worker,
            vmdb2_worker_suite=args.vmdb2_worker_suite,
        )
    elif cloud:
        created, out = new_ubuntu_cloud(
            architecture=architecture,
            default_dir=default_dir,
            out=out,
            qemu_image_size=qemu_image_size,
            suite=suite,
            uri=uri,
            vendor=vendor,
        )
    else:
        created, out = new(
            apt_key=apt_key,
            apt_key_package=apt_key_package,
            architecture=architecture,
            archive_access=archive_access,
            components=components,
            default_dir=default_dir,
            kernel_package=kernel_package,
            include=include,
            merged_usr=_merged_usr,
            out=out,
            qemu_image_size=qemu_image_size,
            storage=storage,
            suite=suite,
            uri=uri,
            vmdebootstrap_options=vmdebootstrap_options,
            vmdebootstrap_worker=vmdebootstrap_worker,
            vmdebootstrap_worker_suite=vmdebootstrap_worker_suite,
        )

    try:
        with VirtWorker(
            ['qemu', created],
            archive_access=archive_access,
            storage=storage,
            suite=suite,
        ) as worker:
            worker.set_up_apt()
            worker.check_call(
                archive_access.get_proxy_env() + ['apt-get', '-y', 'update'])
            worker.check_call([
                'env',
                'DEBIAN_FRONTEND=noninteractive',
            ] + archive_access.get_proxy_env() + [
                'apt-get',
                '-y',
                '--no-install-recommends',
                'install',

                'python3',
                'sbuild',
                'schroot',
            ])
    except Exception:
        if keep:
            if created != out + '.new':
                os.rename(created, out + '.new')
        else:
            os.remove(created)

        raise
    else:
        os.rename(created, out)
