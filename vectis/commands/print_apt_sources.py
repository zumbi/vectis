# Copyright © 2019 Simon McVittie
# SPDX-License-Identifier: GPL-2.0+
# (see vectis/__init__.py)

from vectis.apt import (
    iter_apt_sources,
)
from vectis.error import (
    ArgumentError,
)


def run(
    args,
):
    if args.suite is None:
        if args.default_suite is not None:
            args.override('suite', args.default_suite)
        else:
            raise ArgumentError('--suite must be specified')

    for source in iter_apt_sources(
        args.suite,
        archive_access=args.get_archive_access(),
        components=args.components,
    ):
        print(str(source))
